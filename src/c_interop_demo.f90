module c_interop_demo
    use iso_c_binding, only: c_int

    implicit none
    private
    public :: beggining

    interface
        subroutine middle(answer) bind(C)
            import :: c_int

            implicit none

            integer(c_int), intent(in) :: answer
        end subroutine
    end interface
contains
    subroutine beggining()
        call middle(42_c_int)
    end subroutine

    subroutine ending(answer) bind(C)
        integer(c_int), intent(in) :: answer

        print *, "The answer is: ", answer
    end subroutine
end module
